#!/usr/bin/env ruby

require 'bibtex'
require 'fileutils'
require 'set'

FILE = ARGV.shift

KEYS = Set.new

def gen_key(entry)
  lastname = entry.author.first.last.to_s.gsub(/[^a-zA-Z]/, '').downcase
  key = lastname[0...3] + entry.year.to_s[2...4]
  if KEYS.include? key
    key += "a"
  end
  while KEYS.include? key
    key[-1] = key[-1].next
  end
  KEYS << key
  key
end

bib = BibTeX.open(FILE)
clean_bib = BibTeX::Bibliography.new

bib.each do |entry|
  clean_bib << begin
    case entry
    when BibTeX::String
      entry
    when BibTeX::Entry
      clean = BibTeX::Entry.new
      needed   = ['author', 'year', 'title', 'type']
      expected = ['doi', 'booktitle']
      allowed  = ['journal', 'url', 'number', 'series', 'edition', 'volume']
      needed.each do |key|
        clean.send(key+"=", (entry.send(key) or raise "")) rescue raise "Missing #{key} in #{entry.key}"
      end
      expected.each do |key|
        clean.send(key+"=", (entry.send(key) or raise "")) rescue puts "Missing #{key} in #{entry.key}"
      end
      allowed.each do |key|
        clean.send(key+"=", (entry.send(key) or raise "")) rescue nil
      end
      clean.key = gen_key(entry)
      clean
    else
      raise "Unknown entry type #{entry.inspect}"
    end
  end
end

FileUtils.mv(FILE, FILE + ".bkp")
clean_bib.save_to(FILE)
